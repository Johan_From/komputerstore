// Imports
import { loadAPI } from "./Components/API.js";
import { bank } from "./Components/Bank.js";
import { toPositive, priceOfComputer } from "./Components/Utils.js";

// API URL
const apiURL = "https://noroff-komputer-store-api.herokuapp.com/";

// Buttons 
const btnGetLoan = document.getElementById("buttonGetLoan");
const btnBank = document.getElementById("bankButton");
const btnWork = document.getElementById("workButton");
const btnBuyNow = document.getElementById("buyNowButton")
const btnRepayLoan = document.getElementById("repayLoanButton");

// Dropdown Menu
const dropdownLaptops = document.getElementById("laptops");

// API info
const features = document.getElementById("features");
const computerImage = document.querySelector("img");
const computerInfo = document.getElementById("computerInfo");
const computerName = document.getElementById("computerName");
const computerPrice = document.getElementById("computerPrice");

// Money and user info
const name = document.getElementById("firstLastName");
const payAmount = document.getElementById("payAmount");
let balance = document.getElementById("balanceAmount");
let loanAmount = document.getElementById("loanAmount");

// Array for computers
let computers = new Array();

// Set API data 
const addComputersToMenu = (computers) => {
    /**
     * Used [0] for first item in Array
     */
    computers.forEach(computer => addComputerToMenu(computer));
    computerPrice.innerText = `${computers[0].price} NOK`;
    computerName.innerText = computers[0].title;
    computerImage.src = apiURL + computers[0].image;
    
    const specs = computers[0].specs;
    features.innerHTML = `
        </br><span> Features: </span> </br>
        <span> ${specs[0]} </span> </br>
        <span> ${specs[1]} </span> </br>
        <span> ${specs[2]} </span> </br>
    `;
    /**
        <span> ${specs[3]} </span> </br>
        <span> ${specs[4]} </span> </br>
    */
        
    computerInfo.innerText = `${computers[0].description}`;
}

// Method for filling put the <select></select>
const addComputerToMenu = (computer) => {
    const computerElement = document.createElement("option");
    computerElement.value = computer.id;
    computerElement.appendChild(document.createTextNode(computer.title));
    dropdownLaptops.appendChild(computerElement);
}

// Method for when the menu changes index
const handleComputerMenuChange = (e) => {
    const selectedComputer = computers[e.target.selectedIndex];
    computerPrice.innerText = `${selectedComputer.price} NOK`;
    computerName.innerText = selectedComputer.title;

    // To avoid image error on that specific image
    if(apiURL + selectedComputer.image === "https://noroff-komputer-store-api.herokuapp.com/assets/images/5.jpg"){
        computerImage.src = "https://noroff-komputer-store-api.herokuapp.com/assets/images/5.png";
    }else{
        computerImage.src = apiURL + selectedComputer.image;
    }
    
    features.innerHTML = `
        </br><span>Features:</span></br>
        <span> ${selectedComputer.specs[0]} </span> </br>
        <span> ${selectedComputer.specs[1]} </span> </br>
        <span> ${selectedComputer.specs[2]} </span> </br>
    `;
    /*
        <span> ${selectedComputer.specs[3]} </span> </br>
        <span> ${selectedComputer.specs[4]} </span> </br>
    */ 
    computerInfo.innerText = selectedComputer.description;
}

// Method for setting the start values
const setStartValues = () => {
    name.innerText = `${bank.firstName} ${bank.lastName}`
    balance.innerHTML = `Balance: ${bank.userBalance} Kr.`;
    payAmount.innerHTML = `Pay: ${bank.workMoney} Kr.`;
    btnRepayLoan.style.display = "none";
}

// Method for getting the loan
const getLoan = () => {
    // Check if you have a loan or the input > userBalance * 2 
    if(bank.hasLoan == true){
        alert("You already have a loan! Pay of the loan first!");
    }
    else{
        let inputLoanAmount = prompt("How much money will you loan?");
        if(inputLoanAmount === null){
            return;
        }
        else if(inputLoanAmount > bank.userBalance * 2 ){
            alert("You can't get a loan twice or more then the amount of your balance!");
        }
        else if(isNaN(inputLoanAmount)){
            alert("You must enter an number for amount!");
        }
        else if(inputLoanAmount < 0){
            alert("You cannot have a negative loan amount!");
        }
        else if(inputLoanAmount == 0){
            alert("You cannot loan 0 Kr.!");
        }
        else{
            bank.hasLoan = true;
            let loan = parseFloat(inputLoanAmount);
            bank.userBalance += loan;
            bank.loanAmount += loan;
            balance.innerHTML = `Balance: ${bank.userBalance} Kr.`;
            loanAmount.innerHTML = `Loan: ${bank.loanAmount} Kr.`;
            btnRepayLoan.style.display = "block";
            loanAmount.style.display = "block";
        }
    }
}

// Method for getting work money and displaying it
const getWorkMoney = () => {
    bank.workMoney += 100;
    payAmount.innerText = `Pay: ${bank.workMoney}`; 
}

const transferSalary = () => {
    // If you have a loan
    if(bank.hasLoan == true){
        bank.loanAmount -= bank.transfer10Percent(bank.workMoney); // Transfer to loan debt 

        loanAmount.innerHTML = `Loan: ${bank.loanAmount} Kr.`;
        // When loanAmount is 0
        if(bank.loanAmount == 0){
            bank.userBalance += bank.transfer90Percent(bank.workMoney); // last 90% to you
            balance.innerText = `Balance: ${bank.userBalance} Kr.`;
            payAmount.innerText = "Pay: 0 Kr.";
            removeLoan();
        }
        // If you have money over from the 10% transfer
        else if(bank.loanAmount < 0){
            let calcBack = bank.overFromTransfer(bank.workMoney, bank.loanAmount)
            bank.userBalance += calcBack;
            balance.innerText = `Balance: ${bank.userBalance} Kr.`;
            payAmount.innerText = "Pay: 0 Kr.";
            removeLoan();
        }
        // Transfer as usual
        else{
            loanAmount.innerHTML = `Loan: ${bank.loanAmount} Kr.`;
            bank.userBalance += bank.transfer90Percent(bank.workMoney);
            bank.workMoney = 0;
            payAmount.innerText = `Pay: 0 Kr.`;
            balance.innerText = `Balance: ${bank.userBalance} Kr.`;
        }
    }
    else{
        // Add money as usual
        bank.userBalance += bank.workMoney;
        bank.workMoney = 0;
        payAmount.innerText = "Pay: 0 Kr.";
        balance.innerText = `Balance: ${bank.userBalance} Kr.`;
    }   
}

const repayLoan = () => {
    // Loan calc
    bank.loanAmount -= bank.workMoney
    // If you repay loan and have over money
    if(bank.loanAmount < 0){
        // Convert negative to positive
        let over = toPositive(bank.loanAmount)
        bank.userBalance += over;
        balance.innerText = `Balance: ${bank.userBalance} Kr.`;
        // Reset values
        payAmount.innerText = "Pay: 0 Kr.";
        removeLoan();
    }
    else if(bank.loanAmount == 0){
        payAmount.innerText = "Pay: 0 Kr.";
        removeLoan();
    }
    else{
        loanAmount.innerText = `Loan: ${bank.loanAmount} Kr.`;
        bank.workMoney = 0;
        payAmount.innerText = "Pay: 0 Kr.";
    }
}

// Small method for removing loan
const removeLoan = () => {
    bank.workMoney = 0;
    bank.loanAmount = 0;
    btnRepayLoan.style.display = "none";
    bank.hasLoan = false;
    loanAmount.style.display = "none";
}

// Method for buying a computer
const buyComputer = () => {
    // Get the value inside the computerPrice element then get the first value before whitespace
    let str = computerPrice.innerText;
    let price = priceOfComputer(str);

    // If you have enough or more money
    let proceedWithPurchase = confirm("Do you want to buy the laptop?")
    if(proceedWithPurchase === true){
        if(bank.userBalance >= price){
            bank.userBalance -= price;
            balance.innerHTML = `Balance: ${bank.userBalance} Kr.`;
            alert("You are now the owner of the Laptop!");
        }else{
            alert("You don't have enough money!");
        }
    }else{
        return;
    }
}

// Event listener for page has loaded
document.addEventListener("DOMContentLoaded", () => {
    // Call the API the set the data
    loadAPI(apiURL)
        .then(response => response)
        .then(data => computers = data)
        .then(computers => addComputersToMenu(computers))
    
    // Initialize start values
    setStartValues();
})

// Event listeners for menu and buttons
dropdownLaptops.addEventListener("change", handleComputerMenuChange);
btnGetLoan.addEventListener("click", getLoan);
btnWork.addEventListener("click", getWorkMoney)
btnBank.addEventListener("click", transferSalary)
btnRepayLoan.addEventListener("click", repayLoan);
btnBuyNow.addEventListener("click", buyComputer)