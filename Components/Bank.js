class Person {
    constructor(firstName, lastName, hasLoan){
        this.firstName = firstName,
        this.lastName = lastName,
        this.hasLoan = hasLoan
    }
}

class Bank extends Person{
    constructor(firstName, lastName, hasLoan, userBalance, workMoney, loanAmount) {
        super(firstName, lastName, hasLoan);

        this.userBalance = userBalance,
        this.workMoney = workMoney,
        this.loanAmount = loanAmount
    }

    overFromTransfer = (workMoney, loanAmount) => (workMoney * 0.9) + (Math.abs(loanAmount)); 
    
    transfer90Percent = value => value * 0.9;

    transfer10Percent = value => value * 0.1;
    
}

export const bank = new Bank(
    "Johan",
    "From",
    false,
    200,
    0,
    0
)