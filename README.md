<h1>KomputerStorer</h1>
<hr><p>An JavaScript application that uses an API for fetching computers. The functionality that you can add money, pay of loans, transfer money and pay for a computer from the api. </p><h2>General Information</h2>
<hr><ul>
<li>The app was built with vanilla JavaScript and CSS for styling</li>
<li>The app's purpose is to utilize JavaScript and CSS for a small single page application.</li>

</ul><h2>Technologies Used</h2>
<hr><ul>
<li>JavaScript</li>
</ul><ul>
<li>CSS</li>
</ul><h2>Setup</h2>
<hr><p>To get the app started follow the steps below:</p>

```
$ git clone https://gitlab.com/Johan_From/komputerstore.git
$ cd komputerstore
```
<p>Then open with live server (extenstion in vscode).</p>

<h2>Collaborators</h2>
<ul>
<li><a href="https://gitlab.com/Johan_From" target="_blank">Johan From</a></li>
</ul>
