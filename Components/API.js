export const loadAPI = async (url) => {
    try{
        const endpoint = "computers"
        const resp = await fetch(url+endpoint)
        const post = await resp.json()
        return post;
    }catch(e){
        console.log(e.message);
    }
}