export const toPositive = (value) => {
    return Math.abs(value);
}

export const priceOfComputer = (string) => {
    return parseFloat(string.substring(0, string.indexOf(' ')))
}